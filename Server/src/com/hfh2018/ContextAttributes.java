package com.hfh2018;

import com.google.gson.Gson;

import java.util.ArrayList;

public class ContextAttributes {
    public String getMapImages() {
        Gson gson = new Gson();
        ArrayList<String> mapImageList = new ArrayList<>();
        mapImageList.add("mapImage1.jpg");
        return gson.toJson(mapImageList);
    }
}
