import org.tc33.jheatchart.HeatChart;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.*;
import java.io.File;
import java.io.IOException;

public class HeatChart1 {
    public static void main(String[] args) throws IOException {
        File imageFile = new File("C:\\Users\\VIVIsectI\\Git\\h4h2018\\Mapper\\map.png");
        BufferedImage bufferedBackground = ImageIO.read(imageFile);
        int width = bufferedBackground.getWidth();
        int height = bufferedBackground.getHeight();
        System.out.println(bufferedBackground.getType());


        double[][] data = new double[width][height];
        HeatChart map = new HeatChart(data);

        map.setChartMargin(0);
        map.setShowXAxisValues(false);
        map.setShowYAxisValues(false);
        map.setAxisThickness(0);
        map.setCellSize(new Dimension(5, 5));
        //map.setLowValueColour(new Color(0, 0, 0, 0));

        Image chartImage = map.getChartImage(true);
        BufferedImage bufferedChart = toBufferedImage(chartImage);

        BufferedImage outputImage = new BufferedImage(width, height, bufferedBackground.getType());
        Image transpImg = TransformGrayToTransparency(bufferedChart);
        outputImage = ApplyTransparency(bufferedBackground, transpImg);
        Graphics2D outputGraphic = outputImage.createGraphics();
        outputGraphic.drawImage(bufferedBackground, 0, 0, null);
        //outputGraphic.drawImage(bufferedChart, 0, 0, null);

        System.out.println("outputImage.getType()" + outputImage.getType());
        System.out.println("bufferedBackground.getType()" + bufferedBackground.getType());

        File outFile = new File("C:\\Users\\VIVIsectI\\Git\\h4h2018\\Mapper\\output.jpg");
        ImageIO.write(outputImage, "jpg", outFile);

        // backround map of asu
        // fighur out line
        // figour out couple lines
        // figoure out colors
    }

    /**
     * Converts a given Image into a BufferedImage
     *
     * @param img The Image to be converted
     * @return The converted BufferedImage
     */
    private static BufferedImage toBufferedImage(Image img) {
        if (img instanceof BufferedImage) {
            return (BufferedImage) img;
        }

        // Create a buffered image with transparency
        BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

        // Draw the image on to the buffered image
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();

        // Return the buffered image
        return bimage;
    }

    private static Image TransformGrayToTransparency(BufferedImage image) {
        ImageFilter filter = new RGBImageFilter() {
            public final int filterRGB(int x, int y, int rgb) {
                return (rgb << 8) & 0xFF000000;
            }
        };

        ImageProducer ip = new FilteredImageSource(image.getSource(), filter);
        return Toolkit.getDefaultToolkit().createImage(ip);
    }

    private static BufferedImage ApplyTransparency(BufferedImage image, Image mask) {
        BufferedImage dest = new BufferedImage(
                image.getWidth(), image.getHeight(),
                BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = dest.createGraphics();
        g2.drawImage(image, 0, 0, null);
        AlphaComposite ac = AlphaComposite.getInstance(AlphaComposite.DST_IN, 1.0F);
        g2.setComposite(ac);
        g2.drawImage(mask, 0, 0, null);
        g2.dispose();
        return dest;
    }

}
